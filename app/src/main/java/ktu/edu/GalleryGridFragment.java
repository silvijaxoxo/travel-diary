package ktu.edu;

import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.nostra13.universalimageloader.core.ImageLoader;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;

import ktu.edu.Utils.AllJpgFiles;
import ktu.edu.Utils.JpgFile;

public class GalleryGridFragment extends Fragment {

    private static final String TAG = "GalleryGridFragment";
    private static final int NUM_GRID_COLUMNS = 3;
    private View mView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_gallery_grid,container,false);

        initImageLoader();

        mView = view;
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //setupGrid();
        viewCreated = true;
    }

    boolean viewCreated = false;

    private void initImageLoader(){
        UniversalImageLoader universalImageLoader = new UniversalImageLoader(getActivity());
        ImageLoader.getInstance().init(universalImageLoader.getConfig());
    }

    public static ArrayList<String> imgURLs = new ArrayList<>();
    public static void updateGrid(){
        imgURLs = new ArrayList<>();
        for (JpgFile file : AllJpgFiles.getInstance().getJpgFiles()){
            imgURLs.add(file.getId());
        }

    }
    public void setupGrid(){
        setupImageGrid(imgURLs);
    }

    private String mAppend = "file:/";
    private void setupImageGrid(ArrayList<String> imgURLs){

        if (viewCreated) {
            GridView gridView = mView.findViewById(R.id.gridView);

            int gridWidth = getResources().getDisplayMetrics().widthPixels;
            int imageWidth = gridWidth / NUM_GRID_COLUMNS;
            gridView.setColumnWidth(imageWidth);

            GridImageAdapter adapter = new GridImageAdapter(getActivity(),
                    R.layout.layout_grid_image_view_selection, mAppend, imgURLs);
            adapter.isImageOpenAllowed = true;
            gridView.setAdapter(adapter);

        /*gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("AA", "Click at position " + position);
            }
        });
        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("AA", "LONG Click at position " + position);
                adapter.isSelectionActive = true;
                adapter.selectedPosts.add(position);
                gridView.setAdapter(adapter);
                return false;
            }
        });*/

        }
    }
}
