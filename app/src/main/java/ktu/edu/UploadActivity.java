package ktu.edu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

public class UploadActivity extends AppCompatActivity {

    private static final int NUM_GRID_COLUMNS = 5;

    private ImageButton backBtn;
    private ImageButton nextBtn;

    private GridView gridView;
    private ImageView galleryImage;
    private ProgressBar mProgressBar;
    private Spinner directorySpinner;

    private ArrayList<String> directories;
    private String mAppend = "file:/";
    private String mSelectedImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        backBtn = findViewById(R.id.closeUploadButton);
        backBtn.setOnClickListener(v -> finish());

        nextBtn = findViewById(R.id.nextButton);
        nextBtn.setOnClickListener(v -> {
            Intent intent = new Intent(UploadActivity.this, AddActivity.class);
            intent.putExtra(getString(R.string.selected_image), mSelectedImage);
            startActivity(intent);
        });

        galleryImage = findViewById(R.id.galleryImageView);
        gridView = findViewById(R.id.gridView);
        directorySpinner = findViewById(R.id.spinnerDirectory);
        mProgressBar = findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.GONE);

        directories = new ArrayList<>();

        init();
    }

    private void init(){
        FilePaths filePaths = new FilePaths();

        //check for other folders inside "/storage/emulated/0/pictures"
        if(FileSearch.getDirectoryPaths(filePaths.PICTURES) != null){
            directories = FileSearch.getDirectoryPaths(filePaths.PICTURES);
        }
        directories.add(filePaths.CAMERA);

        Map<String, File> externalLocations = FileSearch.getAllStorageLocations();
        File sdCard = externalLocations.get(FileSearch.SD_CARD);
        if(FileSearch.getDirectoryPaths(sdCard.getAbsolutePath()) != null){
            for (String filePath : FileSearch.getDirectoryPaths(sdCard.getAbsolutePath())){
                directories.add(filePath);
            }
        }

        File[] aDirArray = ContextCompat.getExternalFilesDirs(UploadActivity.this, null);
        File path = new File(aDirArray[1], Environment.DIRECTORY_DCIM);
        String data=path.toString();
        int inde=data.indexOf("Android");
        String subdata=data.substring(0,inde);
        directories.add(subdata+"DCIM/Camera");

        File directory = new File("/storage");
        File[] files = directory.listFiles();
        for(File file : files){
            if(file.isDirectory()) {

                if(file.listFiles() != null)
                {
                    if(FileSearch.getDirectoryPaths(file.getAbsolutePath()) != null ){
                        for (String filePath : FileSearch.getDirectoryPaths(file.getAbsolutePath())){
                            directories.add(filePath);
                            if(FileSearch.containsFolders(new File(filePath))){
                                if(FileSearch.getDirectoryPaths(new File(filePath).getAbsolutePath()) != null ){
                                    for (String filePath2 : FileSearch.getDirectoryPaths(new File(filePath).getAbsolutePath())){

                                        directories.add(filePath2);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        ArrayList<String> directoryNames = new ArrayList<>();
        for(int i = 0; i < directories.size(); i++){
            int index = directories.get(i).lastIndexOf("/") + 1;
            String string = directories.get(i).substring(index);
            directoryNames.add(string);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, directoryNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        directorySpinner.setAdapter(adapter);

        directorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //setup our images grid for the directory chosen
                setupGridView(directories.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setupGridView(String selectedDirectory){
        final ArrayList<String> imgURLs = FileSearch.getFilePaths(selectedDirectory);

        //set the grid column width
        int gridWidth = getResources().getDisplayMetrics().widthPixels;
        int imageWidth = gridWidth/NUM_GRID_COLUMNS;
        gridView.setColumnWidth(imageWidth);

        //use the grid adapter to adapt images to gridview
        GridImageAdapter adapter = new GridImageAdapter(this,
                R.layout.layout_grid_image_view,mAppend, imgURLs);
        gridView.setAdapter(adapter);

        //set the first image to be displayed
        setImage(imgURLs.get(0), galleryImage, mAppend);
        mSelectedImage = imgURLs.get(0);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setImage(imgURLs.get(position),galleryImage,mAppend);
                mSelectedImage = imgURLs.get(position);
            }
        });
    }

    private void setImage(String imgURL, ImageView image, String append){
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(UploadActivity.this));
        imageLoader.displayImage(append + imgURL, image, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                mProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                mProgressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                mProgressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                mProgressBar.setVisibility(View.INVISIBLE);
            }
        });
    }
}
