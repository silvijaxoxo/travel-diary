package ktu.edu;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import ktu.edu.Utils.AllJpgFiles;

public class GalleryFragment extends Fragment {

    private static final String TAG = "GalleryFragment";

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private PostAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_gallery,container,false);

        mRecyclerView = view.findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(getContext());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());

        mLayoutManager = linearLayoutManager;
        adapter = new PostAdapter(AllJpgFiles.getInstance().getJpgFiles());

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(adapter);
        return view;
    }

    public void restartRecycler(){
        adapter = new PostAdapter(AllJpgFiles.getInstance().getJpgFiles());
        mRecyclerView.setAdapter(adapter);
    }
}
