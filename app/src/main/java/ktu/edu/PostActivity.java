package ktu.edu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.squareup.picasso.Picasso;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.constants.ExifTagConstants;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import ktu.edu.Utils.AllJpgFiles;
import ktu.edu.Utils.JpgFile;

public class PostActivity extends AppCompatActivity {

    private Toolbar toolbar;

    private ImageView imageResource;
    private TextView date;
    private TextView address;
    private TextView coordinates;
    private TextView description;

    private ImageButton backBtn;

    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        toolbar = findViewById(R.id.postBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        backBtn = findViewById(R.id.backToMainBtn);
        backBtn.setOnClickListener(v -> {

            String prev = getIntent().getStringExtra("PREVIOUS_ACTIVITY");
            Intent intent = new Intent(PostActivity.this, MainActivity.class);

            GoogleSignInAccount googleSignInAccount = MainActivity.gsia;
            intent.putExtra(MainActivity.GOOGLE_ACCOUNT, googleSignInAccount);

            if (prev != null) {
                if (prev.equals("AddActivity")) {
                    startActivity(intent);
                    finish();
                } else {
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivityIfNeeded(intent, 0);
                    finish();
                }
            } else {
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivityIfNeeded(intent, 0);
                finish();
            }
        });

        url = getIntent().getStringExtra("POST_ID");
        if (url != null){
            setContentValues(url);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.post_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case  R.id.shareBtn:
                Intent intentShareFile = new Intent(Intent.ACTION_SEND);
                String myFilePath = url;
                File fileWithinMyDir = new File(myFilePath);

                if(fileWithinMyDir.exists()) {
                    intentShareFile.setType("image/jpeg");
                    Uri photoUri = FileProvider.getUriForFile(PostActivity.this,
                            "ktu.edu.provider", fileWithinMyDir);
                    intentShareFile.putExtra(Intent.EXTRA_STREAM, photoUri);
                    intentShareFile.putExtra(Intent.EXTRA_SUBJECT, "Sharing File...");
                    intentShareFile.putExtra(Intent.EXTRA_TEXT, "Sharing File...");

                    startActivity(Intent.createChooser(intentShareFile, "Share File"));
                }
                return true;
            case  R.id.editBtn:
                Toast.makeText(PostActivity.this,"Not implemented.",
                        Toast.LENGTH_SHORT).show();
                return true;
            case  R.id.deleteBtn:
                AlertDialog.Builder builder = new AlertDialog.Builder(PostActivity.this);
                builder.setCancelable(false);
                builder.setMessage("Are you sure you want to delete this?")
                        .setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    File file = new File(url);
                    boolean deleted = file.delete();
                    if (deleted){
                        Toast.makeText(PostActivity.this, "File deleted!",
                                Toast.LENGTH_SHORT).show();
                        AllJpgFiles.getInstance().remove(url);

                        Intent intent = new Intent(PostActivity.this, MainActivity.class);
                        GoogleSignInAccount googleSignInAccount = MainActivity.gsia;
                        intent.putExtra(MainActivity.GOOGLE_ACCOUNT, googleSignInAccount);
                        startActivity(intent);
                    } else {
                        Toast.makeText(PostActivity.this, "Error deleting file!",
                                Toast.LENGTH_SHORT).show();
                    }

                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        }
    };

    private void setContentValues(String url){

        imageResource = findViewById(R.id.imageShown);
        date = findViewById(R.id.dateText);
        address = findViewById(R.id.addressPostActivity);
        coordinates = findViewById(R.id.coordinatesPostActivity);
        description = findViewById(R.id.descriptionText);

        JpgFile jpgFile = AllJpgFiles.getInstance().getJpgByURL(url);

        try{
            File f = new File(jpgFile.getId());
            Picasso.with(this).load(f).into(imageResource);
            ImageMetadata metadata = Imaging.getMetadata(f);
            if (metadata != null){
                JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;

                /**--Location--**/
                TiffImageMetadata.GPSInfo gpsInfo = jpegMetadata.getExif().getGPS();
                if(gpsInfo != null){
                    address.setText(jpgFile.getAddress());
                    String coord = gpsInfo.getLatitudeAsDegreesNorth() + ", "
                            + gpsInfo.getLongitudeAsDegreesEast();
                    coordinates.setText(coord);
                } else {
                    address.setText("Unknown address");
                    coordinates.setText("Unknown coordinates");
                }

                /**--Date--**/
                DateFormat format = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss",
                        Locale.getDefault());
                String[] dataDig = jpegMetadata.getExif()
                        .getFieldValue(ExifTagConstants.EXIF_TAG_DATE_TIME_DIGITIZED);
                if(dataDig != null){
                    date.setText(sdf.format(format.parse(dataDig[0])));
                } else {
                    date.setText("Unknown date");
                }

                /**--Description--**/
                String desc = jpegMetadata.getExif()
                        .getFieldValue(ExifTagConstants.EXIF_TAG_USER_COMMENT);
                if(desc != null){
                    description.setText(desc);
                } else {
                    description.setVisibility(View.GONE);
                }
            } else {
                findViewById(R.id.imageData).setVisibility(View.GONE);
            }
        } catch (ImageReadException | IOException | ParseException e) {
            e.printStackTrace();
        }
    }
}
