package ktu.edu;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GestureDetectorCompat;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;

import ktu.edu.Utils.AllJpgFiles;

public class GridImageAdapter extends ArrayAdapter<String> {
    private Context mContext;
    private LayoutInflater mInflater;
    private int layoutResource;
    private String mAppend;
    private ArrayList<String> imgURLs;

    public GridImageAdapter(Context context, int layoutResource, String append, ArrayList<String> imgURLs){
        super(context, layoutResource, imgURLs);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
        this.layoutResource = layoutResource;
        mAppend = append;
        this.imgURLs = imgURLs;

        gestureDetector = new GestureDetectorCompat(context, new SingleTapConfirm());
    }

    private static class ViewHolder{
        SquareImageView image;
        ProgressBar mProgressBar;
        CheckBox mCheckBox;
    }

    private GestureDetectorCompat gestureDetector;

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        final ViewHolder holder;

        if(convertView == null){
            convertView = mInflater.inflate(layoutResource,parent,false);
            holder=new ViewHolder();
            holder.mCheckBox = (CheckBox) convertView.findViewById(R.id.checkbox);
            if (isImageOpenAllowed) {
                holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            if (!selectedPosts.contains(position)) {
                                selectedPosts.add(position);
                            }
                        } else {
                            if (selectedPosts.contains(position)) {
                                selectedPosts.remove(Integer.valueOf(position));
                            }
                        }
                    }
                });
            }
            holder.mProgressBar = (ProgressBar) convertView.findViewById(R.id.gridImageProgressBar);
            holder.image = (SquareImageView) convertView.findViewById(R.id.gridImageView);

            if (isImageOpenAllowed){
                if (isSelectionActive) {
                    holder.image.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            // Always returns false, the gestureDetector does not detect anything
                            //boolean ret = gestureDetector.onTouchEvent(event);
                            // At least the onTouch-callback gets called with the correct position
                            //Log.e("AA", "onTouch returned " + ret + " at position " + position);
                            openPosition = position;
                            return true;
                        }
                    });
                } else {
                    holder.image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Log.e("AA", "Click at position " + position);
                            Intent intent = new Intent(getContext(), PostActivity.class);
                            //Log.e("AA", "Click at position " + AllJpgFiles.getInstance().getJpgByIndex(position).getId());
                            intent.putExtra("POST_ID", AllJpgFiles.getInstance().getJpgByIndex(position).getId());
                            getContext().startActivity(intent);
                        }
                    });
                }
            }

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }
        String imgURL = getItem(position);

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage(mAppend + imgURL, holder.image, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                if(holder.mProgressBar != null){
                    holder.mProgressBar.setVisibility(View.VISIBLE);
                }
                if(holder.mCheckBox != null){
                    if (!isSelectionActive) {
                        holder.mCheckBox.setVisibility(View.GONE); //INVISIBLE
                    }
                    else{
                        holder.mCheckBox.setVisibility(View.VISIBLE);
                        if (selectedPosts.contains(position)){
                            holder.mCheckBox.setChecked(true);
                        } else {
                            holder.mCheckBox.setChecked(false);
                        }
                    }
                }
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                if(holder.mProgressBar != null){
                    holder.mProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                if(holder.mProgressBar != null){
                    holder.mProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                if(holder.mProgressBar != null){
                    holder.mProgressBar.setVisibility(View.GONE);
                }
            }
        });
        return convertView;
    }

    public boolean isSelectionActive = false;
    public ArrayList<Integer> selectedPosts = new ArrayList<>();
    public boolean isImageOpenAllowed = false;
    private int openPosition;

    private class SingleTapConfirm extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapConfirmed(MotionEvent event) {
            //Log.e("AA", "onSingleTapConfirmed"); // never called..
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            //Log.e("AA", "onDoubleTap"); // never called..
            if (openPosition > -1){
                Log.e("AA DOUBLE", AllJpgFiles.getInstance().getJpgByIndex(openPosition).getId());
                Log.e("AA", AllJpgFiles.getInstance().getJpgByIndex(openPosition).getId());
                // open Post Activity
                // put Extra openPosition
            }
            return super.onDoubleTap(e);
        }
    }
}
