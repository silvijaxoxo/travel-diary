package ktu.edu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Environment;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ktu.edu.Utils.AllJpgFiles;
import ktu.edu.Utils.DriveServiceHelper;
import ktu.edu.Utils.JpgFile;


public class MainActivity extends AppCompatActivity implements FilterDialog.FilterListener {

    private static final String TAG = "MainActivity.java";
    public static final String GOOGLE_ACCOUNT = "google_account";

    private TextView profileName;
    private ImageView profilePic;

    private Toolbar topBar;
    private CustomViewPager viewPager;
    private FloatingActionButton upload;
    private List<ImageButton> viewPostsButtonList = new ArrayList<>();
    private ImageButton filterByLocation, filterByDate;

    public static DriveServiceHelper driveServiceHelper;

    private boolean doneSettingFiles = false;
    public static GoogleSignInAccount gsia;
    Thread thread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        topBar = findViewById(R.id.topAppBar);
        setSupportActionBar(topBar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        setupViewPager();
        setupBottomAppBar();

        setDriver();
        thread = new Thread(() -> {
            setupFiles();
            AllJpgFiles.getInstance()
                    .setOriginalFilesArray(AllJpgFiles.getInstance().getJpgFiles());
            doneSettingFiles = true;

            runOnUiThread(() -> {
                Fragment page = getSupportFragmentManager()
                        .findFragmentByTag("android:switcher:" +
                                R.id.container + ":" + viewPager.getCurrentItem());
                if (viewPager.getCurrentItem() == 0 && page != null) {
                    ((MapFragment)page).updateMap();
                }
            });
        });
        thread.start();
    }

    private void setDriver(){
        GoogleSignInAccount googleSignInAccount = getIntent().getParcelableExtra(GOOGLE_ACCOUNT);

        if(googleSignInAccount != null){
            GoogleAccountCredential credential = GoogleAccountCredential
                    .usingOAuth2(MainActivity.this,
                            Collections.singleton(DriveScopes.DRIVE_FILE));
            credential.setSelectedAccount(googleSignInAccount.getAccount());
            Drive googleDriveService = new Drive.Builder(
                    AndroidHttp.newCompatibleTransport(),
                    new GsonFactory(),
                    credential)
                    .setApplicationName("Travel Diary")
                    .build();

            driveServiceHelper = new DriveServiceHelper(googleDriveService);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        profileName = findViewById(R.id.profileText);
        profilePic = findViewById(R.id.cardView);
        gsia = getIntent().getParcelableExtra(GOOGLE_ACCOUNT);
        setDataOnView();
    }

    private void setDataOnView() {
        GoogleSignInAccount googleSignInAccount = gsia;
        Picasso.with(this).load(googleSignInAccount.getPhotoUrl()).centerInside().fit().into(profilePic);
        profileName.setText(googleSignInAccount.getDisplayName());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu,menu);
        return true;
    }

    private void setupViewPager(){
        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new MapFragment());
        adapter.addFragment(new GalleryGridFragment());
        adapter.addFragment(new GalleryFragment());

        viewPager = findViewById(R.id.container);
        viewPager.setAdapter(adapter);
    }

    private void setupBottomAppBar(){

        ImageButton map = findViewById(R.id.mapBtn);
        ImageButton galleryGrid = findViewById(R.id.galleryGridBtn);
        ImageButton gallery = findViewById(R.id.galleryLinearBtn);

        viewPostsButtonList.add(map);
        viewPostsButtonList.add(galleryGrid);
        viewPostsButtonList.add(gallery);

        for(int i =0; i<viewPostsButtonList.size(); i++){
            setOnClick(viewPostsButtonList, i);
        }

        upload = findViewById(R.id.fab);

        upload.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, UploadActivity.class);
            startActivity(intent);
        });

        filterByLocation = findViewById(R.id.filterByLocationBtn);
        filterByDate = findViewById(R.id.filterByDateBtn);

        filterByLocation.setOnClickListener(v -> {
            if (doneSettingFiles) {
                FilterDialog filterDialog = new FilterDialog();
                filterDialog.setLocationAndDate(filterLocation, filterDate1, filterDate2);
                filterDialog.show(getSupportFragmentManager(), "Filter dialog");
            } else {
                Toast.makeText(this, "Please wait until all the files have been loaded",
                        Toast.LENGTH_SHORT).show();
            }
        });

        filterByDate.setOnClickListener(v -> {
            if (doneSettingFiles) {
                FilterDialog filterDialog = new FilterDialog();
                filterDialog.setLocationAndDate(filterLocation, filterDate1, filterDate2);
                filterDialog.show(getSupportFragmentManager(), "Filter dialog");
            } else {
                Toast.makeText(this, "Please wait until all the files have been loaded",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    int galleryClickCount = 0;
    private void setOnClick(final List<ImageButton> btnList, final int i) {
        btnList.get(i).setOnClickListener(v -> {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = getTheme();

            for (int j = 0; j < btnList.size(); j++) {
                if (j == i) {
                    theme.resolveAttribute(android.R.attr.textColorPrimary,
                            typedValue, true);
                    TypedArray arr =
                            MainActivity.this.obtainStyledAttributes(typedValue.data,
                                    new int[]{android.R.attr.textColorPrimary});
                    int color = arr.getColor(0, -1);
                    arr.recycle();
                    btnList.get(j).setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
                    viewPager.setCurrentItem(i, true);
                    if(j==0){   // map fragment is selected
                        Fragment page = getSupportFragmentManager()
                                .findFragmentByTag("android:switcher:" +
                                        R.id.container + ":" + viewPager.getCurrentItem());
                        if (viewPager.getCurrentItem() == j && page != null) {
                            ((MapFragment)page).resetMap();
                            ((MapFragment)page).updateMap();
                        }
                    }
                    if(j==1){   // gallery GRID fragment is selected
                        Fragment page = getSupportFragmentManager()
                                .findFragmentByTag("android:switcher:" +
                                        R.id.container + ":" + viewPager.getCurrentItem());
                        if (viewPager.getCurrentItem() == j && page != null) {
                            ((GalleryGridFragment)page).setupGrid();
                        }
                    }
                    if(j==2){
                        galleryClickCount++;
                        if(galleryClickCount >= 2){
                            Fragment page = getSupportFragmentManager()
                                    .findFragmentByTag("android:switcher:" +
                                            R.id.container + ":" + viewPager.getCurrentItem());
                            if (viewPager.getCurrentItem() == j && page != null) {
                                ((GalleryFragment)page).restartRecycler();
                            }
                        }
                    } else {
                        galleryClickCount = 0;
                    }
                } else {
                    theme.resolveAttribute(android.R.attr.textColorSecondary,
                            typedValue, true);
                    TypedArray arr =
                            MainActivity.this.obtainStyledAttributes(typedValue.data, new int[]{
                                    android.R.attr.textColorSecondary});
                    int primaryColor = arr.getColor(0, -1);
                    arr.recycle();
                    btnList.get(j).setColorFilter(primaryColor, PorterDuff.Mode.SRC_ATOP);
                }
            }
        });
    }

    void setupFiles(){
        String root = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File myDir = new File(root + "/Travel Diary");
        if (myDir.exists()) {
            final ArrayList<String> imgURLs = FileSearch.getFilePaths(myDir.getPath());

            AllJpgFiles.getInstance().deleteAll();
            for (String str : imgURLs){
                AllJpgFiles.getInstance().addNewJpg(str, this);
                int temp = AllJpgFiles.getInstance().getLastJpgIndex();
                JpgFile f = AllJpgFiles.getInstance().getJpgByIndex(temp);
                if (f.getDate() == null || f.getLatLng() == null){
                    AllJpgFiles.getInstance().removeJpgAtIndex(temp);
                }
                AllJpgFiles.getInstance().sortByDate();
                GalleryGridFragment.updateGrid();
                Fragment page = getSupportFragmentManager()
                        .findFragmentByTag("android:switcher:" +
                                R.id.container + ":" + viewPager.getCurrentItem());
                if (viewPager.getCurrentItem() == 1 && page != null) {
                    ((GalleryGridFragment)page).setupGrid();
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.settingsButton:
                Toast.makeText(MainActivity.this,"Not implemented.",
                        Toast.LENGTH_SHORT).show();
                return true;
            case  R.id.signOutButton:
                LoginActivity.GetGoogleClient().signOut().addOnCompleteListener(task -> {
                    Intent intent=new Intent(MainActivity.this,LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                });
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void applyFilter(String location, String dateStart, String dateEnd) {
        ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setTitle("Filtering images");
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        //FILTER
        if (AllJpgFiles.getInstance().filterByLocationAndDate(location, dateStart, dateEnd)) {

            GalleryGridFragment.updateGrid();
            Fragment page = getSupportFragmentManager()
                    .findFragmentByTag("android:switcher:" +
                            R.id.container + ":" + viewPager.getCurrentItem());
            if (viewPager.getCurrentItem() == 1 && page != null) {
                ((GalleryGridFragment) page).setupGrid();
            }
            if (viewPager.getCurrentItem() == 0 && page != null) {
                ((MapFragment)page).resetMap();
                ((MapFragment)page).updateMap();
            }
            if (viewPager.getCurrentItem() == 2 && page != null) {
                ((GalleryFragment)page).restartRecycler();
            }

            filterLocation = location;
            filterDate1 = dateStart;
            filterDate2 = dateEnd;

            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = getTheme();
            theme.resolveAttribute(android.R.attr.textColorPrimary, typedValue, true);
            TypedArray arr =
                    MainActivity.this.obtainStyledAttributes(typedValue.data, new int[]{
                            android.R.attr.textColorPrimary});
            int color = arr.getColor(0, -1);
            arr.recycle();

            if (!location.equals("-")) {
                filterByLocation.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
            }
            if (!dateStart.equals("-") || !dateEnd.equals("-")) {
                filterByDate.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
            }
            Toast.makeText(MainActivity.this,"Filter applied!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(MainActivity.this,"Error applying filter!", Toast.LENGTH_SHORT).show();
        }
        progressDialog.dismiss();
    }

    String filterLocation = "-", filterDate1= "-", filterDate2="-";

    @Override
    public void cancelFilter() {

        ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setTitle("Removing filter");
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        //Remove filter: display original jpg array instead of filtered, update map, grid, recycler
        AllJpgFiles.getInstance().resetJpgFilesArray();
        GalleryGridFragment.updateGrid();
        Fragment page = getSupportFragmentManager()
                .findFragmentByTag("android:switcher:" +
                        R.id.container + ":" + viewPager.getCurrentItem());
        if (viewPager.getCurrentItem() == 1 && page != null) {
            ((GalleryGridFragment)page).setupGrid();
        }
        if (viewPager.getCurrentItem() == 0 && page != null) {
            ((MapFragment)page).resetMap();
            ((MapFragment)page).updateMap();
        }
        if (viewPager.getCurrentItem() == 2 && page != null) {
            ((GalleryFragment)page).restartRecycler();
        }

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getTheme();
        theme.resolveAttribute(android.R.attr.textColorSecondary, typedValue, true);
        TypedArray arr =
                MainActivity.this.obtainStyledAttributes(typedValue.data, new int[]{
                        android.R.attr.textColorSecondary});
        int color = arr.getColor(0, -1);
        arr.recycle();
        filterByLocation.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        filterByDate.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);

        filterLocation = "-";
        filterDate1= "-";
        filterDate2="-";

        progressDialog.dismiss();
        Toast.makeText(MainActivity.this,"Filter removed!", Toast.LENGTH_SHORT).show();
    }
}
