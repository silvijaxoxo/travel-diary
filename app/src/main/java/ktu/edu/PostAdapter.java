package ktu.edu;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;

import ktu.edu.Utils.AllJpgFiles;
import ktu.edu.Utils.JpgFile;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostViewHolder>  {

    private ArrayList<JpgFile> mPostList;
    private ArrayList<JpgFile> mPostListFull;

    public static class PostViewHolder extends RecyclerView.ViewHolder{

        public SquareImageView image;
        public TextView address;
        public TextView date;
        public TextView description;
        public ImageButton locationBtn;
        public ProgressBar mProgressBar;

        public PostViewHolder(View itemView){
            super(itemView);

            image = itemView.findViewById(R.id.imageView);
            address = itemView.findViewById(R.id.addressRecycler);
            date = itemView.findViewById(R.id.dateRecycler);
            description = itemView.findViewById(R.id.descriptionTextView);
            locationBtn = itemView.findViewById(R.id.locationButton);
            mProgressBar = itemView.findViewById(R.id.recyclerImageProgressBar);
        }
    }

    public PostAdapter(ArrayList<JpgFile> postList){
        mPostList = postList;
        mPostListFull = new ArrayList<>(mPostList);
    }

    private Context context;

    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_item, parent, false);

        context = parent.getContext();

        PostViewHolder pvh = new PostViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, final int position) {

        JpgFile currentFile = AllJpgFiles.getInstance().getJpgByIndex(position);

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        imageLoader.displayImage("file:/"+ currentFile.getId(), holder.image, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                holder.mProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                holder.mProgressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                holder.mProgressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                holder.mProgressBar.setVisibility(View.INVISIBLE);
            }
        });

        holder.image.setOnClickListener(v -> {
            Intent intent = new Intent(context, PostActivity.class);
            intent.putExtra("POST_ID", currentFile.getId());
            context.startActivity(intent);
        });

        holder.address.setText(currentFile.getAddress());
        holder.date.setText(currentFile.getDate());
        holder.locationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // SHOW MAP AND MOVE CAMERA TO THAT POSITION
                Toast.makeText(context,"Not implemented.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPostList.size();
    }
}
