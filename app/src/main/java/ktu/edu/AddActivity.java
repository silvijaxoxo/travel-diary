package ktu.edu;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.jpeg.exif.ExifRewriter;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.constants.ExifTagConstants;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputDirectory;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputSet;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ktu.edu.Utils.AllJpgFiles;
import ktu.edu.Utils.DriveServiceHelper;
import ktu.edu.Utils.MapDialog;

public class AddActivity extends AppCompatActivity implements MapDialog.MapDialogListener {

    final String TAG = "AddActivity.java";
    private final Calendar mCalendar = Calendar.getInstance();

    private ImageView mImageResource;
    private EditText mLocation, mDescription, mDate;
    private TextView mLocationTextCreated, mDateTextCreated;
    private TextView mLocationError, mDateError;
    private ImageButton backBtn, addBtn;
    private ImageButton addLocationBtn, cancelLocationBtn;
    private ImageButton addDateBtn, cancelDateBtn;

    private String mAppend = "file:/";
    private String mImageName;

    private MapDialog mapDialog;

    private LatLng mLatLng;
    private LatLng origLatLng;
    private Date mDateTime;
    private Date origDate;

    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    String imgPath = "";
    DriveServiceHelper driveServiceHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        mImageResource = findViewById(R.id.imageSelected);

        mImageName = getIntent().getStringExtra(getString(R.string.selected_image));

        //UniversalImageLoader.setImage(mImageName, mImageResource, null, mAppend);
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        imageLoader.displayImage(mAppend + mImageName, mImageResource, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        });

        mLocationTextCreated = findViewById(R.id.exifLocation);
        mLocation = findViewById(R.id.locationText);
        mLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mLocationError.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(mLocation.getText().toString().length() != 0){
                    if(isLatLngValid(mLocation.getText().toString())){
                        String[] values = mLocation.getText().toString().trim().split(",");

                        mLatLng = new LatLng(Float.valueOf(values[0].trim()),
                                Float.valueOf(values[1].trim()));
                        mLocationError.setVisibility(View.GONE);
                    } else {
                        mLocationError.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        addLocationBtn = findViewById(R.id.buttonLocation);
        addLocationBtn.setOnClickListener(v -> {
            mapDialog = new MapDialog();
            mapDialog.setLatLng(mLatLng);
            mapDialog.setCancelable(false);
            mapDialog.show(getSupportFragmentManager(), "Map dialog");
        });
        cancelLocationBtn = findViewById(R.id.cancelLocation);
        cancelLocationBtn.setOnClickListener(v -> {
            mLocation.setText("");
            mLatLng = origLatLng;
        });
        mLocationError = findViewById(R.id.errorLocation);
        mLocationError.setVisibility(View.GONE);

        mDateTextCreated = findViewById(R.id.exifData);
        mDate = findViewById(R.id.dateText);
        mDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mDateError.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(mDate.getText().toString().length() != 0){
                    if(isDateValid(mDate.getText().toString())){
                        mDateError.setVisibility(View.GONE);
                        try {
                            mDateTime = sdf.parse(mDate.getText().toString());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    } else {
                        mDateError.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        addDateBtn = findViewById(R.id.buttonDate);
        addDateBtn.setOnClickListener(v -> {
            DatePickerDialog dateDialog = new DatePickerDialog(AddActivity.this);
            dateDialog.setOnDateSetListener((view, year, monthOfYear, dayOfMonth) -> {
                mCalendar.set(Calendar.YEAR, year);
                mCalendar.set(Calendar.MONTH, monthOfYear);
                mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                mDate.setText(sdf.format(mCalendar.getTime()));
                Toast.makeText(AddActivity.this,
                        "Select date applied", Toast.LENGTH_SHORT).show();
            });
            dateDialog.setCancelable(false);

            dateDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
                    "Cancel", (dialog, which) -> {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            Toast.makeText(AddActivity.this,
                                    "Select date canceled", Toast.LENGTH_SHORT).show();
                        }
                    });

            dateDialog.show();
        });
        cancelDateBtn = findViewById(R.id.cancelDate);
        cancelDateBtn.setOnClickListener(v -> mDate.setText(""));
        mDateError = findViewById(R.id.errorDate);
        mDateError.setVisibility(View.GONE);

        mDescription = findViewById(R.id.descriptionText);

        backBtn = findViewById(R.id.backButton);
        backBtn.setOnClickListener(v -> finish());

        addBtn = findViewById(R.id.addButton);
        addBtn.setOnClickListener(v -> {

            if(mDateError.getVisibility() != View.VISIBLE
                    && mLocationError.getVisibility() != View.VISIBLE){

                ProgressDialog progressDialog = new ProgressDialog(AddActivity.this);
                progressDialog.setTitle("Uploading photo");
                progressDialog.setMessage("Please wait...");
                progressDialog.show();

                addPhoto(); // saves image in internal storage
                AllJpgFiles.getInstance().addNewJpg(imgPath, this);

                File temp = new File(imgPath);
                if(temp.exists()) {
                    driveServiceHelper.uploadImage(progressDialog, AddActivity.this, temp);
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(AddActivity.this,
                            "Error uploading image!", Toast.LENGTH_SHORT).show();
                    returnToMain();
                }
            }
            else {
                Toast.makeText(AddActivity.this,
                        "All errors must be fixed!", Toast.LENGTH_SHORT).show();
            }
        });

        setupMetadata(mImageName);
        driveServiceHelper = MainActivity.driveServiceHelper;
    }

    private void returnToMain(){
        Intent intent = new Intent(AddActivity.this, MainActivity.class);
        startActivity(intent);
    }

    private void setupMetadata(String file){
        try{
            File f = new File(file);

            ImageMetadata metadata = Imaging.getMetadata(f);
            if (metadata != null){
                JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;

                /**--Location--**/
                TiffImageMetadata.GPSInfo gpsInfo = jpegMetadata.getExif().getGPS();
                if(gpsInfo != null){
                    origLatLng = new LatLng(gpsInfo.getLatitudeAsDegreesNorth(),
                            gpsInfo.getLongitudeAsDegreesEast());
                    DecimalFormat df = new DecimalFormat("##.######");
                    String str = df.format(origLatLng.latitude)
                            + ", " + df.format(origLatLng.longitude);
                    mLocationTextCreated.setText(str);
                } else {
                    origLatLng = getCurrentLatLng();
                    mLocationTextCreated.setVisibility(View.GONE);
                }

                /**--Date--**/
                DateFormat format = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss",
                        Locale.getDefault());
                String[] dataOrig = jpegMetadata.getExif()
                        .getFieldValue(ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL);
                if (dataOrig != null){
                    origDate = format.parse(dataOrig[0]);
                    mDateTextCreated.setText(sdf.format(origDate));
                } else {
                    mDateTextCreated.setVisibility(View.GONE);
                }
                String[] dataDig = jpegMetadata.getExif()
                        .getFieldValue(ExifTagConstants.EXIF_TAG_DATE_TIME_DIGITIZED);
                if(dataDig != null && !Arrays.equals(dataOrig, dataDig)){
                    Date temp = format.parse(dataDig[0]);
                    mDate.setText(sdf.format(temp));
                }

                /**--Description--**/
                String description = jpegMetadata.getExif()
                        .getFieldValue(ExifTagConstants.EXIF_TAG_USER_COMMENT);
                if(description != null){
                    mDescription.setText(description);
                }
            } else {
                mLocationTextCreated.setVisibility(View.GONE);
                mDateTextCreated.setVisibility(View.GONE);
                origLatLng = getCurrentLatLng();
            }
            mLatLng = origLatLng;
        } catch (ImageReadException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private boolean isLatLngValid(String text){
        Pattern pattern = Pattern.compile("^-?([1-8]?\\d(\\.\\d+)?|90(\\.0+)?)," +
                "\\s*[-+]?(180(\\.0+)?|((1[0-7]\\d)|([1-9]?\\d))(\\.\\d+)?)$");
        Matcher matcher = pattern.matcher(text);

        if (matcher.find())
            return true;
        else
            return false;
    }

    private boolean isDateValid(String text){
        Pattern pattern = Pattern.compile(
                "^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$"
                        + "|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$"
                        + "|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$"
                        + "|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$");
        Matcher matcher = pattern.matcher(text);

        if (matcher.find())
            return true;
        else
            return false;
    }

    private LatLng getCurrentLatLng(){
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return new LatLng(0, 0);
        }
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        return new LatLng(location.getLatitude(), location.getLongitude());
    }

    @Override
    public void applyPlaceCoordinates() {
        mLatLng = mapDialog.getLatLng();
        mLocation.setText(mapDialog.printLatLng());
    }

    private void addPhoto(){
        File f = new File(mImageName);
        Uri contentUri = Uri.fromFile(f);
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver() , contentUri);
            saveImage(bitmap, f);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveImage(Bitmap finalBitmap, File jpeg){
        String root = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File myDir = new File(root + "/Travel Diary");
        if (!myDir.exists()) {
            if (!myDir.mkdirs()) {
                Log.i(TAG, "Failed to create storage directory.");
            } else {
                Log.i(TAG, "Directory created.");
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fName = "IMG_"+ timeStamp +".jpg";

        File file = new File(myDir, fName); //Log.i(TAG, "FilePath: "+file.getPath());

        imgPath = file.getPath();

        if (file.exists()) {
            file.delete ();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();

            /**--Scan media to display image in gallery--**/
            MediaScannerConnection.scanFile(this,
                    new String[] { imgPath/*myDir.getPath()*/ }, new String[] { "image/jpeg" }, null);

        } catch (Exception e) {
            e.printStackTrace();
        }

        File exifVar =  new File(myDir.getPath(), "IMG_" + timeStamp + ".jpg");

        File dst = new File(exifVar.getAbsolutePath());

        try (FileOutputStream fos = new FileOutputStream(dst);
                 OutputStream os = new BufferedOutputStream(fos)) {

            TiffOutputSet outputSet = null;

            ImageMetadata metadata = Imaging.getMetadata(jpeg);
            JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;

            if (null != jpegMetadata) {
                final TiffImageMetadata exif = jpegMetadata.getExif();

                if (null != exif){
                    outputSet = exif.getOutputSet();
                }
            }
            if (null == outputSet){
                outputSet = new TiffOutputSet();
            }

            final TiffOutputDirectory exifDirectory = outputSet.getOrCreateExifDirectory();

            /**--LOCATION--**/
            outputSet.setGPSInDegrees(mLatLng.longitude, mLatLng.latitude);

            /**--DATE--**/
            String dateDig;

            if(mDate.getText().toString().length() != 0){
                dateDig = new SimpleDateFormat("yyyy:MM:dd")
                        .format(sdf.parse(mDate.getText().toString())) + " 00:00:00";
            } else {
                Date currentDate = new Date(System.currentTimeMillis());
                dateDig = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss").format(currentDate);
            }
            if(origDate == null) {
                exifDirectory.removeField(ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL);
                exifDirectory.add(ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL, dateDig);
            }
            if (origDate == null && mDate.getText().toString().length() == 0) {
                exifDirectory.removeField(ExifTagConstants.EXIF_TAG_DATE_TIME_DIGITIZED);
                exifDirectory.add(ExifTagConstants.EXIF_TAG_DATE_TIME_DIGITIZED, dateDig);
            } else if (origDate != null && mDate.getText().toString().length() == 0){
                exifDirectory.removeField(ExifTagConstants.EXIF_TAG_DATE_TIME_DIGITIZED);
                exifDirectory.add(ExifTagConstants.EXIF_TAG_DATE_TIME_DIGITIZED,
                        jpegMetadata.getExif()
                                .getFieldValue(ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL));
            } else {
                exifDirectory.removeField(ExifTagConstants.EXIF_TAG_DATE_TIME_DIGITIZED);
                exifDirectory.add(ExifTagConstants.EXIF_TAG_DATE_TIME_DIGITIZED, dateDig);
            }

            /**--DESCRIPTION--**/
            if(mDescription.getText().toString().length() != 0){
                exifDirectory.removeField(ExifTagConstants.EXIF_TAG_USER_COMMENT);
                exifDirectory.add(ExifTagConstants.EXIF_TAG_USER_COMMENT,
                        mDescription.getText().toString());
            }

            new ExifRewriter().updateExifMetadataLossless(jpeg, os, outputSet);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ImageReadException e) {
            e.printStackTrace();
        } catch (ImageWriteException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}