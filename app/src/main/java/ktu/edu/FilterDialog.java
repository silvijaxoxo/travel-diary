package ktu.edu;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import ktu.edu.Utils.AllJpgFiles;
import ktu.edu.Utils.JpgFile;

public class FilterDialog extends AppCompatDialogFragment {

    private FilterListener listener;

    private Spinner location;
    private TextView dateStart;
    private TextView dateEnd;
    private ImageButton addStartDate;
    private ImageButton addEndDate;
    private ImageButton removeStartDate;
    private ImageButton removeEndDate;

    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private final Calendar mCalendar = Calendar.getInstance();

    String countryName;

    String country, date1, date2;

    public void setLocationAndDate(String location, String dateStart, String dateEnd){
        country = location;
        date1 = dateStart;
        date2 = dateEnd;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View dialogView = inflater.inflate(R.layout.filter, null);

        location = dialogView.findViewById(R.id.filterSpinner);

        ArrayList<String> countryNames = new ArrayList<>();
        countryNames.add("-");
        for(JpgFile f : AllJpgFiles.getInstance().getOriginalFilesArray()){
            String country = f.getCountry();
            if (country != "" && !countryNames.contains(country)){
                countryNames.add(country);
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, countryNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        location.setAdapter(adapter);

        location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                countryName = countryNames.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (country != null && !country.equals("-")) {
            int spinnerPosition = adapter.getPosition(country);
            location.setSelection(spinnerPosition);
        }

        dateStart = dialogView.findViewById(R.id.dateStart);
        dateStart.setText(date1);
        dateEnd = dialogView.findViewById(R.id.dateEnd);
        dateEnd.setText(date2);
        addStartDate = dialogView.findViewById(R.id.filterStartDate);
        addStartDate.setOnClickListener(v -> {
            DatePickerDialog dateDialog = new DatePickerDialog(getContext());
            dateDialog.setOnDateSetListener((view, year, monthOfYear, dayOfMonth) -> {
                mCalendar.set(Calendar.YEAR, year);
                mCalendar.set(Calendar.MONTH, monthOfYear);
                mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                dateStart.setText(sdf.format(mCalendar.getTime()));
                Toast.makeText(getContext(),
                        "\"Date from\" applied", Toast.LENGTH_SHORT).show();
            });
            dateDialog.setCancelable(false);

            dateDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
                    "Cancel", (dialog, which) -> {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            Toast.makeText(getContext(),
                                    "\"Date from\" canceled", Toast.LENGTH_SHORT).show();
                        }
                    });

            dateDialog.show();
        });
        addEndDate = dialogView.findViewById(R.id.filterEndDate);
        addEndDate.setOnClickListener(v -> {
            DatePickerDialog dateDialog = new DatePickerDialog(getContext());
            dateDialog.setOnDateSetListener((view, year, monthOfYear, dayOfMonth) -> {
                mCalendar.set(Calendar.YEAR, year);
                mCalendar.set(Calendar.MONTH, monthOfYear);
                mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                dateEnd.setText(sdf.format(mCalendar.getTime()));
                Toast.makeText(getContext(),
                        "\"Date to\" applied", Toast.LENGTH_SHORT).show();
            });
            dateDialog.setCancelable(false);

            dateDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
                    "Cancel", (dialog, which) -> {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            Toast.makeText(getContext(),
                                    "\"Date to\"  canceled", Toast.LENGTH_SHORT).show();
                        }
                    });

            dateDialog.show();
        });
        removeStartDate = dialogView.findViewById(R.id.cancelStartDate);
        removeStartDate.setOnClickListener(v -> dateStart.setText("-"));
        removeEndDate = dialogView.findViewById(R.id.cancelEndDate);
        removeEndDate.setOnClickListener(v -> dateEnd.setText("-"));

        builder.setView(dialogView)
                .setTitle("Filter")
                .setNegativeButton("Cancel", (dialog, which) -> listener.cancelFilter())
                .setPositiveButton("Filter", (dialog, which) -> {
                    String locationText = countryName;
                    String dateText1 = dateStart.getText().toString();
                    String dateText2 = dateEnd.getText().toString();
                    listener.applyFilter(locationText, dateText1, dateText2);
                });

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (FilterDialog.FilterListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement FilterListener");
        }
    }

    public interface FilterListener {
        void applyFilter(String location, String dateStart, String dateEnd);
        void cancelFilter();
    }
}
