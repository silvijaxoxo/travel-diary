package ktu.edu.Utils;

public class GoogleDriveFileHolder {
    private String id;
    private String name;
    //private DateTime modifiedTime;
    //private DateTime createdTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
