package ktu.edu.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.api.client.http.FileContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Permission;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ktu.edu.AddActivity;
import ktu.edu.MainActivity;
import ktu.edu.PostActivity;

public class DriveServiceHelper {
    //https://www.youtube.com/watch?v=5bSEIQYJzKs&list=PLF0BIlN2vd8sqGfhxmzJ93SmfMLMtM1nW&index=3

    private final Executor mExecutor = Executors.newSingleThreadExecutor();
    private Drive mDriveService;

    private final String appName = "Travel Diary";
    private GoogleDriveFileHolder googleDFH;    // variable used in uploadJpegFile()

    public DriveServiceHelper(Drive drive){
        mDriveService = drive;
    }

    /*String lastCreatedFileId;
    public Task<String> createImageFile(String filePath, String fileName){

        return Tasks.call(mExecutor, () -> {
            File metadata = new File()
                    .setParents(Collections.singletonList("root"))
                    .setName(fileName);

            java.io.File file = new java.io.File(filePath);

            FileContent mediaContent = new FileContent("image/jpeg", file);

            File googleFile = null;

            try{
                googleFile = mDriveService.files().create(metadata,mediaContent).execute();
            } catch (Exception e){
                e.printStackTrace();
            }

            if(googleFile == null){
                throw new IOException("Null result when requesting file creation.");
            }
            lastCreatedFileId = googleFile.getId();
            return googleFile.getId();

        });
    }*/

    public Task<GoogleDriveFileHolder> searchFile(String fileName, String mimeType) {
        return Tasks.call(mExecutor, () -> {

            FileList result = mDriveService.files().list()
                    .setQ("name = '" + fileName + "' and mimeType ='" + mimeType + "'")
                    .setSpaces("drive")
                    .setFields("files(id, name)")
                    .execute();
            GoogleDriveFileHolder googleDriveFileHolder = new GoogleDriveFileHolder();
            if (result.getFiles().size() > 0) {
                //folderId = result.getFiles().get(0).getId();
                googleDriveFileHolder.setId(result.getFiles().get(0).getId());
                googleDriveFileHolder.setName(result.getFiles().get(0).getName());
                //googleDriveFileHolder.setModifiedTime(result.getFiles().get(0).getModifiedTime());
            }

            return googleDriveFileHolder;
        });
    }

    public Task<GoogleDriveFileHolder> searchFolder(String folderName) {
        return Tasks.call(mExecutor, () -> {

            FileList result = mDriveService.files().list()
                    .setQ("mimeType = '" + DriveFolder.MIME_TYPE + "' and name = '" + folderName + "' ")
                    .setSpaces("drive")
                    .execute();
            GoogleDriveFileHolder googleDriveFileHolder = null;
            if (result.getFiles().size() > 0) {
                googleDriveFileHolder = new GoogleDriveFileHolder();
                googleDriveFileHolder.setId(result.getFiles().get(0).getId());
                googleDriveFileHolder.setName(result.getFiles().get(0).getName());
            }
            return googleDriveFileHolder;
        });
    }

    public Task<GoogleDriveFileHolder> createFolder(String folderName, @Nullable String folderId) {
        return Tasks.call(mExecutor, () -> {

            GoogleDriveFileHolder googleDriveFileHolder = new GoogleDriveFileHolder();

            List<String> root;
            if (folderId == null) {
                root = Collections.singletonList("root");
            } else {
                root = Collections.singletonList(folderId);
            }
            File metadata = new File()
                    .setParents(root)
                    .setMimeType(DriveFolder.MIME_TYPE)
                    .setName(folderName);

            File googleFile = mDriveService.files().create(metadata).execute();
            if (googleFile == null) {
                throw new IOException("Null result when requesting file creation.");
            }
            googleDriveFileHolder.setId(googleFile.getId());
            return googleDriveFileHolder;
        });
    }

    public Task<GoogleDriveFileHolder> uploadFile(final java.io.File localFile,
                                                  final String mimeType,
                                                  @Nullable final String folderId) {
        return Tasks.call(mExecutor, () -> {
            // Retrieve the metadata as a File object.
            List<String> root;
            if (folderId == null) {
                root = Collections.singletonList("root");
            } else {

                root = Collections.singletonList(folderId);
            }

            File metadata = new File()
                    .setParents(root)
                    .setMimeType(mimeType)
                    .setName(localFile.getName());

            FileContent fileContent = new FileContent(mimeType, localFile);

            File fileMeta = mDriveService.files().create(metadata, fileContent).execute();
            GoogleDriveFileHolder googleDriveFileHolder = new GoogleDriveFileHolder();
            googleDriveFileHolder.setId(fileMeta.getId());
            googleDriveFileHolder.setName(fileMeta.getName());
            return googleDriveFileHolder;
        });
    }

    /*public Task<GoogleDriveFileHolder> uploadJpegFile(String filePath){

        return Tasks.call(mExecutor, () -> {
            googleDFH = null;

            searchFolder("Travel Diary") // "AAAAATRAVEL"
                    .addOnCompleteListener(task -> {
                        if(task.isSuccessful()){
                            if(task.getResult() != null){
                                Log.i("AddActivity.java3",
                                        "Found folder with name: " + task.getResult().getName() + " ID: " + task.getResult().getId());
                                // turiu folderio id i kuri reikia ikelt paveiksliuka
                                uploadFile(new java.io.File(filePath),"image/jpeg",
                                        task.getResult().getId()).addOnCompleteListener(task3 -> {
                                            if (task3.isSuccessful()){
                                                Log.i("AddActivity.java3", "Success uploading image to Google Drive! ID: " + task3.getResult().getId());
                                                googleDFH = new GoogleDriveFileHolder();
                                                googleDFH.setId(task3.getResult().getId());
                                                googleDFH.setName(task3.getResult().getName());
                                            } else {
                                                Log.i("AddActivity.java3", "Error uploading image to Google Drive!");
                                            }
                                        });
                            } else {
                                Log.i("AddActivity.java3", "Folder NOT found");
                                createFolder("Travel Diary", null)
                                        .addOnCompleteListener(task1 -> {
                                            if(task1.isSuccessful()){
                                                Log.i("AddActivity.java3", "Folder created!");
                                                uploadFile(new java.io.File(filePath),
                                                        "image/jpeg", task.getResult().getId())
                                                        .addOnCompleteListener(task2 -> {
                                                            if (task2.isSuccessful()){
                                                                Log.i("AddActivity.java3", "Success uploading image to Google Drive! ID: " + task2.getResult().getId());
                                                                googleDFH = new GoogleDriveFileHolder();
                                                                googleDFH.setId(task2.getResult().getId());
                                                                googleDFH.setName(task2.getResult().getName());
                                                            } else {
                                                                Log.i("AddActivity.java3", "Error uploading image to Google Drive!");
                                                            }
                                                        });
                                            } else {
                                                Log.i("AddActivity.java3", "Error creating folder!");
                                            }
                                        });
                            }
                        }else {
                            Log.i("AddActivity.java2", "Error uploading image to google drive...");
                        }
                    });
            if(googleDFH == null){
                Log.i("AddActivity.java2", "NULL");
            } else {
                Log.i("AddActivity.java2", "Uploaded image id: " + googleDFH.getId());
            }
            return googleDFH;
        });
    }*/

    final String error = "Error uploading image to Google Drive!";
    final String success = "Success uploading image to Google Drive!";
    public void uploadImage(ProgressDialog progressDialog, Context context, java.io.File file){
        searchFolder(appName)
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        if(task.getResult() != null){
                            //Log.i("AddActivity.java3",
                            //        "Found folder with name: " + task.getResult().getName() + " ID: " + task.getResult().getId());
                            uploadFile(file,"image/jpeg",
                                    task.getResult().getId()).addOnCompleteListener(task3 -> {
                                if (task3.isSuccessful()){
                                    progressDialog.dismiss();
                                    //Log.i("AddActivity.java3", "Success uploading image to Google Drive! ID: " + task3.getResult().getId());
                                    Toast.makeText(context, success, Toast.LENGTH_SHORT).show();
                                    goToPost(context);
                                } else {
                                    progressDialog.dismiss();
                                    //Log.i("AddActivity.java3", "Error uploading image to Google Drive!");
                                    Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                                    returnToMain(context);
                                }
                            });
                        } else {
                            //Log.i("AddActivity.java3", "Folder NOT found");
                            createFolder(appName, null)
                                    .addOnCompleteListener(task1 -> {
                                        if(task1.isSuccessful()){
                                            //Log.i("AddActivity.java3", "Folder created!");
                                            uploadFile(file,
                                                    "image/jpeg", task.getResult().getId())
                                                    .addOnCompleteListener(task2 -> {
                                                        if (task2.isSuccessful()){
                                                            progressDialog.dismiss();
                                                            //Log.i("AddActivity.java3", "Success uploading image to Google Drive! ID: " + task2.getResult().getId());
                                                            Toast.makeText(context, success, Toast.LENGTH_SHORT).show();
                                                            goToPost(context);
                                                        } else {
                                                            progressDialog.dismiss();
                                                            //Log.i("AddActivity.java3", "Error uploading image to Google Drive!");
                                                            Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                                                            returnToMain(context);
                                                        }
                                                    });
                                        } else {
                                            progressDialog.dismiss();
                                            Toast.makeText(context, "Error creating app folder!", Toast.LENGTH_SHORT).show();
                                            returnToMain(context);
                                        }
                                    });
                        }
                    }else {
                        progressDialog.dismiss();
                        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                        returnToMain(context);
                    }
                });
    }
    private void returnToMain(Context ctx){
        Intent intent = new Intent(ctx, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        ctx.startActivity(intent);
    }
    private void goToPost(Context ctx){
        Intent intent = new Intent(ctx, PostActivity.class);
        intent.putExtra("PREVIOUS_ACTIVITY", "AddActivity");
        intent.putExtra("POST_ID", AllJpgFiles.getInstance().getJpgByIndex(AllJpgFiles.getInstance().getLastJpgIndex()).getId());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        ctx.startActivity(intent);

    }
    /*
    service.files().trash(fileId).execute();
This is for moving file to trash

service.files().untrash(fileId).execute();
for delete file from trash  on server
    * */
    public Task<List<GoogleDriveFileHolder>> queryFiles(@Nullable final String folderId) {
        return Tasks.call(mExecutor, new Callable<List<GoogleDriveFileHolder>>() {
                    @Override
                    public List<GoogleDriveFileHolder> call() throws Exception {
                        List<GoogleDriveFileHolder> googleDriveFileHolderList = new ArrayList<>();
                        String parent = "root";
                        if (folderId != null) {
                            parent = folderId;
                        }

                        FileList result = mDriveService.files().list().setQ("'" + parent + "' in parents").setFields("files(id, name)").setSpaces("drive").execute();

                        for (int i = 0; i < result.getFiles().size(); i++) {
                            Log.i("AddActivity.java queryFiles", "Id: " +result.getFiles().get(i).getId()+ " Name: " + result.getFiles().get(i).getName());
                            GoogleDriveFileHolder googleDriveFileHolder = new GoogleDriveFileHolder();
                            googleDriveFileHolder.setId(result.getFiles().get(i).getId());
                            googleDriveFileHolder.setName(result.getFiles().get(i).getName());
                            //AllJpgFiles.getInstance().addNewJpg(result.getFiles().get(i).getId()); /****************/
                            googleDriveFileHolderList.add(googleDriveFileHolder);

                        }

                        return googleDriveFileHolderList;
                    }
                }
        );
    }

    /*public void setupAllJpgFiles(){
        searchFolder(appName)
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        if(task.getResult() != null){
                            Log.i("AddActivity.java3","Found folder with name: " + task.getResult().getName() + " ID: " + task.getResult().getId());
                            AllJpgFiles.getInstance().setFolderID(task.getResult().getId());
                            queryFiles(AllJpgFiles.getInstance().getFolderID()).addOnSuccessListener(new OnSuccessListener<List<GoogleDriveFileHolder>>() {
                                @Override
                                public void onSuccess(List<GoogleDriveFileHolder> googleDriveFileHolders) {
                                    Log.i("AddActivity.java3", "Success querying files");
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.i("AddActivity.java3", "Failure querying files");
                                }
                            });
                        } else {
                            Log.i("AddActivity.java3", "Folder NOT found");
                        }
                    }else {
                        Log.i("AddActivity.java2", "Error searching for app folder...");
                    }
                });
    }*/
}
