package ktu.edu.Utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.constants.ExifTagConstants;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class JpgFile implements Comparable {
    private String url;
    private LatLng latLng;
    private String date;
    private String country;
    private String address;
    private boolean isSelected = false;

    private Context mContext;
    public Context getContext() { return mContext;}

    public JpgFile (String url, Context context){
        this.url = url;
        mContext = context;
        setMetadata();
    }

    public JpgFile (String url, Context context, LatLng latLng, String date, String country, String address){
        this.url = url;
        mContext = context;
        this.latLng = latLng;
        this.date = date;
        this.country = country;
        this.address = address;
    }

    public String getId() { return url;}
    public LatLng getLatLng() { return latLng;}
    public String getDate() { return date;}
    public String getCountry() { return country;}
    public String getAddress() { return address;}

    public boolean isSelected() {
        return isSelected;
    }
    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    private void setMetadata(){
        try {
            File f = new File(url);
            ImageMetadata metadata = Imaging.getMetadata(f);
            if (metadata != null){
                JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;
                TiffImageMetadata.GPSInfo gpsInfo = jpegMetadata.getExif().getGPS();
                if(gpsInfo != null){
                    latLng = new LatLng(gpsInfo.getLatitudeAsDegreesNorth(),
                            gpsInfo.getLongitudeAsDegreesEast());
                    getLocation(gpsInfo.getLatitudeAsDegreesNorth(),
                            gpsInfo.getLongitudeAsDegreesEast());
                }
                String[] dataDig = jpegMetadata.getExif()
                        .getFieldValue(ExifTagConstants.EXIF_TAG_DATE_TIME_DIGITIZED);
                if(dataDig != null){
                    Date temp = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss",
                            Locale.getDefault()).parse(dataDig[0]);
                    date = new SimpleDateFormat("yyyy-MM-dd",
                            Locale.getDefault()).format(temp);
                }
            }
        } catch (ImageReadException | IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int compareTo(Object o) {
        return this.getDate().compareTo(((JpgFile) o).getDate());
    }

    private void getLocation(double latitude, double longitude){

        LocationManager locationManager = (LocationManager) mContext
                .getSystemService(Context.LOCATION_SERVICE);
        String provider = locationManager.getBestProvider(new Criteria(), true);

        country = "";
        address = "Unknown Location";

        if (mContext.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && mContext.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location locations = locationManager.getLastKnownLocation(provider);
        List<String> providerList = locationManager.getAllProviders();
        if(null!=locations && providerList.size()>0){

            Geocoder geocoder = new Geocoder(mContext.getApplicationContext(),
                    Locale.getDefault());
            try {
                List<Address> listAddresses = geocoder.getFromLocation(latitude,longitude, 1);
                if(null!=listAddresses&&listAddresses.size()>0){
                    country = listAddresses.get(0).getCountryName();
                    address = listAddresses.get(0).getAddressLine(0);
                    return;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
