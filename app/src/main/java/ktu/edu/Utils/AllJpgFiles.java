package ktu.edu.Utils;

import android.content.Context;

import java.util.ArrayList;
import java.util.Collections;

public class AllJpgFiles {
    private static final AllJpgFiles ourInstance = new AllJpgFiles();

    public static AllJpgFiles getInstance() {
        return ourInstance;
    }

    private ArrayList<JpgFile> jpgFiles = new ArrayList<>();
    public ArrayList<JpgFile> getJpgFiles(){
        return  jpgFiles;
    }

    private ArrayList<JpgFile> allJpgFiles = new ArrayList<>();
    public void setOriginalFilesArray(ArrayList<JpgFile> list) { allJpgFiles = new ArrayList<>(list); }
    public ArrayList<JpgFile> getOriginalFilesArray() { return allJpgFiles; }
    public void resetJpgFilesArray() { jpgFiles = new ArrayList<>(allJpgFiles); }

    public boolean filterByLocationAndDate(String location, String date1, String date2){
        jpgFiles = new ArrayList<>();
        if (location.equals("-") && date1.equals("-") && date2.equals("-")){
            jpgFiles = new ArrayList<>(allJpgFiles);
            return true;
        }

        for (JpgFile f : allJpgFiles){
            if (!location.equals("-")){
                if (!date1.equals("-") && !date2.equals("-")){
                    if (date1.compareTo(date2) == 0){
                        if (f.getCountry().equals(location) && (f.getDate().compareTo(date1) == 0)){
                            jpgFiles.add(new JpgFile(f.getId(), f.getContext(), f.getLatLng(),
                                    f.getDate(), f.getCountry(), f.getAddress()));
                        }
                    } else if (date1.compareTo(date2) < 0){
                        if (f.getCountry().equals(location) && f.getDate().compareTo(date1) >= 0
                                && f.getDate().compareTo(date2) <= 0){
                            jpgFiles.add(new JpgFile(f.getId(), f.getContext(), f.getLatLng(),
                                    f.getDate(), f.getCountry(), f.getAddress()));
                        }
                    }
                } else if (date1.equals("-") && !date2.equals("-")){ // antra data nustatyta
                    if (f.getCountry().equals(location) && f.getDate().compareTo(date2) <= 0){
                        jpgFiles.add(new JpgFile(f.getId(), f.getContext(), f.getLatLng(),
                                f.getDate(), f.getCountry(), f.getAddress()));
                    }
                } else if (!date1.equals("-") && date2.equals("-")){
                    if (f.getCountry().equals(location) && f.getDate().compareTo(date1) >= 0 ){
                        jpgFiles.add(new JpgFile(f.getId(), f.getContext(), f.getLatLng(),
                                f.getDate(), f.getCountry(), f.getAddress()));
                    }
                } else{
                    if (f.getCountry().equals(location)){
                        jpgFiles.add(new JpgFile(f.getId(), f.getContext(), f.getLatLng(),
                                f.getDate(), f.getCountry(), f.getAddress()));
                    }
                }
            } else {
                if (!date1.equals("-") && !date2.equals("-")){
                    if (date1.compareTo(date2) == 0){
                        if ((f.getDate().compareTo(date1) == 0)){
                            jpgFiles.add(new JpgFile(f.getId(), f.getContext(),
                                    f.getLatLng(), f.getDate(), f.getCountry(), f.getAddress()));
                        }
                    } else if (date1.compareTo(date2) < 0){
                        if (f.getDate().compareTo(date1) >= 0 && f.getDate().compareTo(date2) <= 0){
                            jpgFiles.add(new JpgFile(f.getId(), f.getContext(),
                                    f.getLatLng(), f.getDate(), f.getCountry(), f.getAddress()));
                        }
                    } else {
                        jpgFiles = new ArrayList<>(allJpgFiles);
                        return false;
                    }
                } else if (date1.equals("-") && !date2.equals("-")){ // antra data nustatyta
                    if (f.getDate().compareTo(date2) <= 0){
                        jpgFiles.add(new JpgFile(f.getId(), f.getContext(),
                                f.getLatLng(), f.getDate(), f.getCountry(), f.getAddress()));
                    }
                } else if (!date1.equals("-") && date2.equals("-")){
                    if (f.getDate().compareTo(date1) >= 0){
                        jpgFiles.add(new JpgFile(f.getId(), f.getContext(),
                                f.getLatLng(), f.getDate(), f.getCountry(), f.getAddress()));
                    }
                }
            }
        }
        return true;
    }

    public void addNewJpg(String id, Context context){
        JpgFile f = new JpgFile(id, context);
        jpgFiles.add(f);
        allJpgFiles.add(f);
    }
    public void removeJpgAtIndex(int ind){
        jpgFiles.remove(ind);
    }

    public int getLastJpgIndex(){
        return  jpgFiles.size()-1;
    }
    public JpgFile getJpgByIndex(int index){
        return  jpgFiles.get(index);
    }

    public JpgFile getJpgByURL(String url) {

        for (JpgFile file : jpgFiles){
            if (file.getId().equals(url)){
                return file;
            }
        }
        return null;
    }

    public void sortByDate(){
        Collections.sort(jpgFiles,Collections.reverseOrder());
    }

    public void deleteAll(){ jpgFiles = new ArrayList<>();}

    public void remove(String url){
        for (JpgFile file : jpgFiles){
            if (file.getId().equals(url)){
                jpgFiles.remove(file);
                allJpgFiles.remove(file);
                break;
            }
        }
    }
}
