package ktu.edu.Utils;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

import ktu.edu.R;

public class MapDialog extends AppCompatDialogFragment {

    private static String TAG = "MapDialog.java";
    private  MapDialogListener listener;
    private TextView address;
    private TextView fulladdress;
    private TextView coordinates;
    private SquareMap map;

    private LatLng latLng;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.dialogmap, null);

        map = view.findViewById(R.id.mapViewDialog);

        address = view.findViewById(R.id.headlineMapDialog);
        fulladdress = view.findViewById(R.id.addressMapDialog);
        coordinates = view.findViewById(R.id.coordinatesMapDialog);

        MapsInitializer.initialize(getActivity());

        map.onCreate(savedInstanceState);
        map.onResume();
        map.getMapAsync(googleMap -> {

            try {
                boolean success = googleMap.setMapStyle(
                        MapStyleOptions.loadRawResourceStyle(
                                getContext(), R.raw.orange));

                if (!success) {
                    Log.e(TAG, "Style parsing failed.");
                }
            } catch (Resources.NotFoundException e) {
                Log.e(TAG, "Can't find style. Error: ", e);
            }

            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            googleMap.getUiSettings().setZoomControlsEnabled(true);
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);

            googleMap.setOnCameraIdleListener(() -> {
                latLng = new LatLng(googleMap.getCameraPosition()
                        .target.latitude, googleMap.getCameraPosition().target.longitude);

                printLocationName(latLng);
                coordinates.setText(printLatLng());
            });
        });

        builder.setView(view)
                .setTitle("Select image location")
                .setNegativeButton("Cancel", (dialog, which) -> Toast.makeText(getContext(),
                        "Select location canceled",Toast.LENGTH_SHORT).show())
                .setPositiveButton("Ok", (dialog, which) -> {
                    listener.applyPlaceCoordinates();
                    Toast.makeText(getContext(),
                            "Select location applied",Toast.LENGTH_SHORT).show();
                });

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (MapDialog.MapDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement MapDialogListener");
        }
    }

    public interface MapDialogListener {
        void applyPlaceCoordinates();
    }

    public void setLatLng(LatLng latLng){
        this.latLng = latLng;
    }

    public LatLng getLatLng(){
        return latLng;
    }

    public String printLatLng(){
        DecimalFormat df = new DecimalFormat("#.######");
        return df.format(latLng.latitude)+", "+df.format(latLng.longitude);
    }

    private void printLocationName(LatLng location) {
        LocationManager locationManager = (LocationManager) getContext()
                .getSystemService(Context.LOCATION_SERVICE);
        String provider = locationManager.getBestProvider(new Criteria(), true);

        if (getContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && getContext().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            address.setText("Unknown Location");
            fulladdress.setText("Unknown Address");
            return;
        }
        Location locations = locationManager.getLastKnownLocation(provider);
        List<String> providerList = locationManager.getAllProviders();
        if(null!=locations && null!=providerList && providerList.size()>0){

            Geocoder geocoder = new Geocoder(getContext().getApplicationContext(),
                    Locale.getDefault());
            try {
                List<Address> listAddresses = geocoder.getFromLocation(location.latitude,
                        location.longitude, 1);
                if(null!=listAddresses&&listAddresses.size()>0){
                    String _Location = listAddresses.get(0).getAddressLine(0);
                    String knownName = listAddresses.get(0).getFeatureName();
                    String[] values = _Location.split(",");

                    if(values[0].contains(knownName)){
                        String remainder = _Location.substring(_Location.indexOf(",")+2);
                        address.setText(values[0]);
                        fulladdress.setText(remainder);
                    }else{
                        address.setText(values[0] + "," + values[1]);
                        fulladdress.setText(_Location);
                    }
                    return;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        address.setText("Unknown Location");
        fulladdress.setText("Unknown Address");
    }
}
