package ktu.edu.Utils;

import android.content.Context;
import android.util.AttributeSet;

import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;

public class SquareMap extends MapView {
    public SquareMap(Context context) {
        super(context);
    }

    public SquareMap(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SquareMap(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public SquareMap(Context context, GoogleMapOptions googleMapOptions) {
        super(context, googleMapOptions);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
